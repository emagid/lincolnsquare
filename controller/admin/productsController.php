<?php

use Emagid\Html\Form;

class productsController extends adminController
{

    function __construct()
    {
        parent::__construct("Product");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $this->_viewData->start = '';
        $this->_viewData->end = '';
        if (!Empty($_GET['how_many'])) {
            $params['limit'] = $_GET['how_many'];
            $this->_viewData->limit = $_GET['how_many'];
        } else {
            $params['limit'] = 50;
            $this->_viewData->limit = 50;
        }
        $params['queryOptions']['page_size'] = 25;
        parent::index($params);
    }

//    public function import(Array $params = [])
//    {
//
//        $arrResult = array();
//        $handle = fopen($_FILES['csv']['tmp_name'], "r");
//        if ($handle) {
//            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
//                if (empty($data[0])) {
//                } else {
//                    $arrResult[] = $data;
//                }
//
//            }
//            fclose($handle);
//        }
//
//        print_r($arrResult);
//        //
//
//        for ($x = 1; $x < count($arrResult) + 1; $x++) {
//
//            $pr2 = new \Model\Product();
//            $pr2->name = $arrResult[$x][0];
//            $pr2->slug = $arrResult[$x][0];
//            $pr2->sku = $arrResult[$x][0];
//            $pr2->shape = $arrResult[$x][1];
//            $pr2->colors = $arrResult[$x][2];
//            $pr2->clarity = $arrResult[$x][3];
//            $pr2->weight = $arrResult[$x][4];
//            $pr2->lab = $arrResult[$x][5];
//            $pr2->cut_grade = $arrResult[$x][6];
//            $pr2->polish = $arrResult[$x][7];
//            $pr2->symmetry = $arrResult[$x][8];
//            $pr2->fluor = $arrResult[$x][9];
//            $pr2->rapaport_price = $arrResult[$x][10];
//            $pr2->price = $arrResult[$x][11];
//            $pr2->total = $arrResult[$x][12];
//            $pr2->certificate = $arrResult[$x][13];
//            $pr2->length = $arrResult[$x][14];
//            $pr2->width = $arrResult[$x][15];
//            $pr2->depth = $arrResult[$x][16];
//            $pr2->depth_percent = $arrResult[$x][17];
//            $pr2->table_percent = $arrResult[$x][18];
//            $pr2->girdle = $arrResult[$x][19];
//            $pr2->culet = $arrResult[$x][20];
//            $pr2->origin = $arrResult[$x][22];
//            $pr2->memo_status = $arrResult[$x][23];
//            $pr2->inscription = $arrResult[$x][24];
//            $pr2->certificate_file = $arrResult[$x][25];
//
//            $pr2->save();
//        }
//
//
//    }

    public function update(Array $arr = [])
    {
        $pro = new $this->_model(isset($arr['id']) ? $arr['id'] : null);
        $cat = new \Model\Product_Category();
        $this->_viewData->product_categories = $cat->getProductCategories($pro->id, 1);
        //$this->_viewData->product_collections = \Model\Product_Collection::get_product_collections($pro->id);
        //$this->_viewData->product_materials = \Model\Product_Material::get_product_materials($pro->id);
        //$this->_viewData->product_notes = \Model\Note::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
        // foreach($this->_viewData->product_notes as $note){
        // 	$note->admin = \Model\Admin::getItem($note->admin_id);
        // 	$note->insert_time = new DateTime($note->insert_time);
        // 	$note->insert_time = $note->insert_time->format('m-d-Y H:i:s');
        // }
        // $this->_viewData->product_questions = \Model\Question::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
        // foreach($this->_viewData->product_questions as $question){
        // 	$question->insert_time = new DateTime($question->insert_time);
        // 	$question->insert_time = $question->insert_time->format('m-d-Y H:i:s');
        // }
        //$this->_viewData->product_images = [];
        //$this->_viewData->brands = \Model\Brand::getList();
        $this->_viewData->categories = \Model\Category::getList();
        // $this->_viewData->colors = \Model\Color::getList();
        // $this->_viewData->collections = \Model\Collection::getList();
        // $this->_viewData->materials = \Model\Material::getList();

        parent::update($arr);
    }

    //protected function afterObjSave($obj){
    // if (isset($_POST['note'])){
    // 	$note = new \Model\Note();
    // 	$note->name = $_POST['note']['name'];
    // 	$note->description = $_POST['note']['description'];
    // 	$note->admin_id = \Emagid\Core\Membership::userId();
    // 	$note->product_id = $obj->id;
    // 	$note->save();
    // }
    //}

    function upload_images($params)
    {
        header("Content-type:application/json");
        $data = [];
        $data['success'] = false;
        $data['redirect'] = false;
        $id = (isset($params['id']) && is_numeric($params['id']) && $params['id'] > 0) ? $params['id'] : 0;
        if ((int)$id > 0) {
            $product = \Model\Product::getItem($id);
            if ($product == null) {
                $data['redirect'] = true;
            }
            $this->save_upload_images($_FILES['file'], $product->id, UPLOAD_PATH . 'products' . DS, []);
            $data['success'] = true;
        }

        //print_r($_FILES);
        echo json_encode($data);
        exit();
    }

    public function save_upload_images($files, $obj_id, $upload_path, $sizes = [])
    {
        $counter = 1;
        $map = new \Model\Product_Map();
        $mapId = $map->getMap($obj_id, "product")->id;
        // get highest display order counter if any images exist for specified listing
        $image_high = \Model\Product_Image::getItem(null, ['where' => 'product_map_id=' . $mapId, 'orderBy' => 'display_order', 'sort' => 'DESC']);
        if ($image_high != null) {
            $counter = $image_high->display_order + 1;
        }
        foreach ($files['name'] as $key => $val) {
            $product_image = new \Model\Product_Image();
            $temp_file_name = $files['tmp_name'][$key];
            $file_name = uniqid() . "_" . basename($files['name'][$key]);
            $file_name = str_replace(' ', '_', $file_name);
            $file_name = str_replace('-', '_', $file_name);

            $allow_format = array('jpg', 'png', 'gif', 'JPG');
            $filetype = explode("/", $files['type'][$key]);
            $filetype = strtolower(array_pop($filetype));
            //$filetype = explode(".", $files['name'][$key]); //$file['type'] has value like "image/jpeg"
            //$filetype = array_pop($filetype);
            if ($filetype == 'jpeg' || $filetype == "JPG") {
                $filetype = 'jpg';
            }

            if (in_array($filetype, $allow_format)) {

                $img_path = compress_image($temp_file_name, $upload_path . $file_name, 60, $files['size'][$key]);
                if ($img_path === false) {
                    move_uploaded_file($temp_file_name, $upload_path . $file_name);
                }
                //move_uploaded_file($temp_file_name, $upload_path . $file_name);

                if (count($sizes) > 0) {
                    foreach ($sizes as $key => $val) {
                        if (is_array($val) && count($val) == 2) {
                            resize_image_by_minlen($upload_path,
                                $file_name, min($val), $filetype);

                            $path = images_thumb($file_name, min($val),
                                $val[0], $val[1], file_format($file_name));
                            $image_size_str = implode('_', $val);
                            copy($path, $upload_path . $image_size_str . $file_name);
                        }
                    }
                }

                $product_image->image = $file_name;
                $product_image->product_map_id = $mapId;
                $product_image->display_order = $counter;
                $product_image->save();
            }
            $counter++;
        }
    }


    public function search()
    {
        $products = \Model\Product::search($_GET['keywords']);

        echo '[';
        foreach ($products as $key => $product) {
            echo '{ "id": "' . $product->id . '", "name": "' . $product->name . '", "featured_image": "' . $product->featured_image . '", "price":"' . $product->price . '", "mpn":"' . $product->mpn . '" }';
            if ($key < (count($products) - 1)) {
                echo ",";
            }
        }
        echo ']';
    }

    // public function google_product_feed(){
    //        if(isset($_SESSION["google_feed_generated"]) && $_SESSION["google_feed_generated"] == 1){
    //            $this->_viewData->generated = true;
    //            unset($_SESSION["google_feed_generated"]);
    //        }
    //        $this->loadView($this->_viewData);
    //    }

    //  public function google_product_feed_post(){

    //  	$genders = [ 1 => 'unisex', 2 => 'male', 3 => 'female' ];
    //  	$type = "Apparel & Accessories > Jewelry > Watches";
    //  	$condition = "new";
    //      $availability = "in stock"; // "out of stock"
    //      $gpc = 201;
    //      $identifier_exists = "TRUE";
    //      $age_group = "adult";

    //      $f = fopen(UPLOAD_PATH."/google_product_feed.txt","w"); fclose($f); //clear the file
    //      $f = fopen(UPLOAD_PATH."/google_product_feed.txt","a"); //open with append write mode

    //      $header = "id\ttitle\tdescription\tgoogle product category\tlink\timage link\tcondition\tavailability\tprice\tbrand\tmpn\tidentifier exists\tcolor\tage group\tgender\tproduct type";
    //      fwrite($f, $header);

    //      $sql = "SELECT product.id as id, product.name as name, product.description as description, product.slug as slug, ";
    //      $sql .= "product.featured_image as featured_image, product.price as price, product.mpn as mpn, brand.name as brand_name, ";
    //      $sql .= "color.name as color_name, product.gender as gender ";
    //      $sql .= "FROM product ";
    //      $sql .= "INNER JOIN brand ON (product.brand = brand.id) ";
    //      $sql .= "INNER JOIN color ON (product.color = color.id) ";
    //      $sql .= "WHERE product.active = 1 ";

    //      $products = $this->emagid->db->getResults($sql);
    //      $line_items = "";
    //      foreach($products as $product){
    //          $id = $product['id'];
    //          $title = $product['name'];
    //          $description = str_replace('\\', '\\\\', $product['description']);
    //          $description = str_replace('<p>', ' ', $description);
    //          $description = str_replace('</p>', ' ', $description);
    //          $description = str_replace('<br>', ' ', $description);
    //          $description = str_replace('<br />', ' ', $description);
    //          $description = str_replace('<br/>', ' ', $description);
    //          $description = str_replace("\n", " ", $description);
    //          $description = str_replace('\n', ' ', $description);
    //          $description = str_replace("\r", " ", $description);
    //          $description = str_replace('\r', ' ', $description);
    //          $description = str_replace("\t", " ", $description);
    //          $description = str_replace('\t', ' ', $description);
    //          $description = str_replace(PHP_EOL,' ',$description);

    //          $link = "http://www.itmustbetime.com/product/".$product['slug'];

    // $image_link = $product['featured_image'];
    // if (is_null($image_link) || trim($image_link) == '' || !file_exists(UPLOAD_PATH.'products'.DS.$image_link)){
    // 	$image_link = "http://www.itmustbetime.com".ADMIN_IMG.'itmustbetime_watch.png';
    // } else {
    // 	$image_link = "http://www.itmustbetime.com".UPLOAD_URL . 'products/' . $image_link;
    // }

    //          $price = $product['price'] . " USD";
    //          $brand = $product['brand_name'];
    //          $mpn = $product['mpn'];
    //          $color = $product['color_name'];
    //          $gender = $genders[$product['gender']];

    //          $line_item = "\n".$id."\t".$title."\t".$description."\t".$gpc."\t".$link."\t".$image_link."\t".$condition."\t".$availability."\t".$price."\t".$brand."\t".$mpn."\t".$identifier_exists."\t".$color."\t".$age_group."\t".$gender."\t".$type;
    //          fwrite($f, $line_item);
    //      }
    //      fclose($f);
    //      $_SESSION["google_feed_generated"] = 1;
    //      redirect(ADMIN_URL.'products/google_product_feed');
    //  }


}






















