<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 11/5/15
 * Time: 1:16 PM
 */
namespace Model;

class Shipping_Method extends \Emagid\Core\Model {
    static $tablename = "shipping_method";

    public static $fields = [
        'is_default'=>['type'=>'boolean'],
        'name'=>['required'=>true],
        'cart_subtotal_range_min',
        'cost'=>['required'=>true]
    ];

    static function clearDefault($except = 0){
        self::execute('update '.self::$tablename.' set is_default = false where id <> '.$except);
    }
}